import numpy as np

def random_dither(img,ratio=0.5):
    """ Randomly puts black dots in shape """

    if ratio > 1:
        raise ValueError('Ratio needs to be between 0 and 1')
    # make image binary
    out_img = img.copy()
    out_img[out_img>=127] = 255
    out_img[out_img<127] = 0
    assert len(np.unique(out_img)) == 2

    shape_coords = np.where(out_img==255)
    shape_enum = np.arange(0,len(shape_coords[0]))

    idx = np.random.choice(shape_enum,
                           size=int(np.ceil(len(shape_enum)*ratio)),
                           replace=False,
                           p=None)
    
    dot_locs = (shape_coords[0][idx],shape_coords[1][idx])
    out_img[dot_locs] = 0
    
    return out_img
